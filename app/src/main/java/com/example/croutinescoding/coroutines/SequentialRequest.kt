package com.example.croutinescoding.coroutines

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.lang.Exception


class SequentialRequest(
    private val mockApi: MockApi,
    private val viewModelScope: CoroutineScope
) {
    fun sequentialNetworkRequest(){
        viewModelScope.launch {
            val users = try {
                mockApi.getUsers()
                mockApi.getCategories()
            } catch (exception : Exception){
                emptyList<String>()
            }
        }
    }


}