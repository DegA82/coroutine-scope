package com.example.croutinescoding.coroutines

interface MockApi {

    suspend fun getUsers() : List<String>

    suspend fun getCategories() : List<String>

    suspend fun getCategoryList(category: String) : List<String> = emptyList()
}