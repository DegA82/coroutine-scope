package com.example.croutinescoding.coroutines

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class DynamicRequest(
    private val mockApi: MockApi,
    private val viewModelScope: CoroutineScope
) {
    fun dynamicNetworkRequest(){
        viewModelScope.launch {
            mockApi.getCategories().map{mockApi.getCategoryList(it)}
        }
    }
}