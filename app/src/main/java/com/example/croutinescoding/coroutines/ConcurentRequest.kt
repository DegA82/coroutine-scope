package com.example.croutinescoding.coroutines

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.lang.Exception

class ConcurentRequest(
    private val mockApi: MockApi,
    private val viewModelScope: CoroutineScope
) {

    fun performConcurrentNetworkRequest(){

        viewModelScope.launch {
            val users = try {
                mockApi.getUsers()
            } catch (exception : Exception){
                emptyList<String>()
            }
            val categories = try {
                mockApi.getCategories()
            } catch (exception : Exception){
                emptyList<String>()
            }
        }
    }
}