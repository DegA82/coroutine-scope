package com.example.croutinescoding.coroutines

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class PerformSingleNetworkRequest(
    private val mockApi: MockApi,
    private val viewModelScope: CoroutineScope
) {
    fun performSingleNetworkRequest() {
        viewModelScope.launch {
            val users = try {
                mockApi.getUsers()
            } catch (exception: Exception) {
                emptyList<String>()
            }
        }
    }
}